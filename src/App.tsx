import './App.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from './Component/Login/login';
import Students from './Component/Students/students';

function App() {
  return <>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login/>} />
        <Route path="/students" element={<Students/>} />
      </Routes>
    </BrowserRouter>
  </>
}

export default App;
