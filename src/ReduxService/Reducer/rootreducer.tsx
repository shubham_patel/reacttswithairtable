import { Students } from './studentsReducer/studentsReducer';
import { Classes } from './classesReducer/classesReducer';
import { SelectStudent } from './selectStudentsReducer/selectStudentsReducer';
import { combineReducers } from 'redux'

export const rootreducer = combineReducers({Students, Classes, SelectStudent});