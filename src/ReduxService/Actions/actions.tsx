export const Students =(data:any)=>{
    return {
        type:'Students',
        payload:data,
    }
}

export const Classes =(data:any)=>{
    return {
        type:'Classes',
        payload:data,
    }
}

export const SelectStudent =(data:any)=>{
    return {
        type:'SelectStudent',
        payload:data,
    }
}