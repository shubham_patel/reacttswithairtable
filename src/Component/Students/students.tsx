import './students.css'
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector} from 'react-redux';

function Students() {

  const navigate = useNavigate();
  const [classesIds,setClasssesIds] = React.useState([]);
  const [showClasses, setShowClasses] = React.useState([]);

  const studentsData = useSelector((state:any) => state.Students);
  const classesData = useSelector((state:any) => state.Classes);
  const selectStudent = useSelector((state:any) => state.SelectStudent);

  function studentName(data:any) {
    const className = data.fields.Name
    const studentsList = [];
    for(let i=0; i<studentsData?.length; i++){
      for(let j=0; j<data.fields.Students.length; j++){
        if(studentsData[i]?.id === data.fields.Students[j]){
          studentsList.push(studentsData[i].fields.Name)
        }
      }
    }
    return { className: className, studentsList: studentsList}
  }

  React.useEffect(()=>{
    for(let i=0; i<studentsData?.length; i++){
      if(studentsData[i]?.fields?.Name === selectStudent){
        setClasssesIds(studentsData[i].fields.Classes);
      }
    }
  },[selectStudent, studentsData])

  React.useEffect(()=>{
    let arr:any = [];
    for(let i=0; i<classesData?.length; i++){
      for(let j=0; j<classesIds?.length; j++ ){
        if(classesIds[j] === classesData[i]?.id){
          const data:any = studentName(classesData[i]);
          arr.push(data);
        }
      }
    }
    setShowClasses(arr);
  },[classesData, classesIds])

  function logout(){
    navigate("/");
  }
  return (
    <div>
      <div className='logoutButton'>
        <button onClick={logout}>Log Out</button>
      </div>
      {showClasses.map(
        (element:any) => {
          return (
            <div className='box'>
              <h4>Name</h4>
              <p>{element.className}</p>
              <h4>Students</h4>
              <ul className='list-main'>
              {element.studentsList.map((name:any)=>{
                return <>
                  <li>{`${name}, `}</li>
                </>}
              )}
              </ul>
            </div>  
          )
        }
      )}
    </div>
  )
}

export default Students;