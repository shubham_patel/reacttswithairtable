import './login.css';
import React  from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from "react-router-dom";
import { Students, Classes, SelectStudent } from '../../ReduxService/Actions/actions';
import { studentsTable, classesTable } from "../../Apis/apis";

function Login() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [studentName, setStudentName] = React.useState();

  React.useEffect(() => {
    studentsTable.select({view: 'Grid view'})
      .eachPage((records, fetchNextPage) => {
        dispatch(Students(records));
        fetchNextPage();
      })
    classesTable.select({view: 'Grid view'})
      .eachPage((records, fetchNextPage) => {
        dispatch(Classes(records));
        fetchNextPage();
      })
  }, []);

  const handleChange = (event:any) => {
    setStudentName(event.target.value);
  };

  function handelSubmit(){
    dispatch(SelectStudent(studentName));
    navigate("/students");
  }

  return <>
      <div className='login'>
        <label>Student Name:</label>
        <input type='text' placeholder='Enter Student Name' onChange={handleChange}/>
      </div>
      <div className='text-center'>
        <button onClick={handelSubmit}>Login</button>
      </div>
  </>
}

export default Login;